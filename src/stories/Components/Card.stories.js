import Card from "../../components/Card/Card";
import '../../../.storybook/utils.css';

export default {
    title: 'Components/Card',
    component: Card,
    argTypes: {
        name: { control: 'text' },
        description: { control: 'text' },
        image: { control: 'text'}
    },
};

const Template = (args) => ({
    // Components used in your story `template` are defined in the `components` object
    components: { Card },
    // The story's `args` need to be mapped into the template through the `setup()` method
    setup() {
        return { args };
    },
    // And then the `args` are bound to your component with `v-bind="args"`
    template: '<Card v-bind="args" />',
});

export const Primary = Template.bind({});
// More on args: https://storybook.js.org/docs/vue/writing-stories/args
Primary.args = {
    name: 'Pusy Cat',
    description: 'lorem ipsum and much more',
    image: 'https://s3.eu-west-1.amazonaws.com/storymuseum/_1023x682_crop_center-center_80_none/1001_PussyCatPussyCat_MiladaVigerova.jpg'
};

export const Secondary = Template.bind({});
Secondary.args = {
    label: 'Button',
};

export const Large = Template.bind({});
Large.args = {
    size: 'large',
    label: 'Button',
};

export const Small = Template.bind({});
Small.args = {
    size: 'small',
    label: 'Button',
};
